package com.epam.structure.bridge;

public interface Implementator {
    boolean isEnabled();
    void enable();
    void disable();
    int getVolume();
    void setVolume(int volume);
}
