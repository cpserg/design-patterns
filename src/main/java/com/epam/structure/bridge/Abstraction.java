package com.epam.structure.bridge;

public abstract class Abstraction {
    Implementator implementator;

    public Abstraction(Implementator implementator) {
        this.implementator = implementator;
    }

    public void togglePower() {
        if (implementator.isEnabled()) {
            implementator.disable();
        } else {
            implementator.enable();
        }
    }

    public void volumeUp() {
        int oldVolume = implementator.getVolume();
        implementator.setVolume(++oldVolume);
    }

    public void volumeDown() {
        int oldVolume = implementator.getVolume();
        if (oldVolume > 0) {
            implementator.setVolume(--oldVolume);
        }
    }

}
