package com.epam.structure.bridge;

/**
 * Разделяет монолитный код на части
 */
public class Client {
    public static void main(String[] args) {
        Radio radio = new Radio();
        RemoteControl rc = new RemoteControl(radio);
        rc.togglePower();
        rc.volumeUp();
    }
}
