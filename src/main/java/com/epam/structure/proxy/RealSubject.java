package com.epam.structure.proxy;

/**
 * определяет реальный объект, представленный заместителем
 */
public class RealSubject implements Subject {
    @Override
    public void request() {
        System.out.println("real subject");
    }
}
