package com.epam.structure.proxy;

/**
 * Прокси (Сурогат, Заместитель)
 *
 * С помощью паттерна заместитель при доступе к объекту вводится дополнительный уровень косвенности.
 * У этого подхода есть много вариантов в зависимости от вида заместителя
 *
 */
public class Client {
    public static void main(String[] args) {
        Subject bruceWillis = new RealSubject();
        Subject surrogate = new Proxy(bruceWillis);
        surrogate.request();
    }
}
