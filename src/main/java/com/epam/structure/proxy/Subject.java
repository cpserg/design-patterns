package com.epam.structure.proxy;

/**
 * определяет общий для RealSubject и Proxy интерфейс, так что класс
 * Proxy можно использовать везде, где ожидается RealSubject;
 */
public interface Subject {
    void request();
}
