package com.epam.structure.adapter.third_party_service;

/**
 * Сторонняя логика.
 * (Класс или интерфейс)
 */
public class Adaptee {

    public void specificRequest() {
        System.out.println("3-rd party method");
    }

}
