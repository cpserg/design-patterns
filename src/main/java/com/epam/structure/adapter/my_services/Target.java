package com.epam.structure.adapter.my_services;

/**
 * определяет зависящий от предметной области интерфейс, которым пользуется Client
 */
public interface Target {
    void request();
}
