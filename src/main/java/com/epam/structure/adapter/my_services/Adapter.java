package com.epam.structure.adapter.my_services;

import com.epam.structure.adapter.third_party_service.Adaptee;

/**
 * адаптирует интерфейс Adaptee к интерфейсу Target
 */
public class Adapter implements Target {
    Adaptee adaptee = new Adaptee();

    @Override
    public void request() {
        System.out.println("call 3-rd party service");
        adaptee.specificRequest();
    }
}
