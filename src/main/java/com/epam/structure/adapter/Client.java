package com.epam.structure.adapter;

import com.epam.structure.adapter.my_services.Adapter;
import com.epam.structure.adapter.my_services.Target;

public class Client {

    public static void main(String[] args) {
        Target target = new Adapter();
        target.request();
    }
}
