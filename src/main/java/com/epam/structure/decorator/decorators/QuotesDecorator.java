package com.epam.structure.decorator.decorators;

import com.epam.structure.decorator.Component;

/**
 * возлагает дополнительные обязанности на компонент
 */
public class QuotesDecorator extends Decorator {

    public QuotesDecorator(Component component) {
        super(component);
    }

    @Override
    public void doOperation() {
        System.out.print("\"");
        super.doOperation();
        System.out.print("\"");
    }

}
