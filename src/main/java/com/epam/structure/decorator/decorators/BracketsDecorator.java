package com.epam.structure.decorator.decorators;

import com.epam.structure.decorator.Component;

/**
 * возлагает дополнительные обязанности на компонент
 */
public class BracketsDecorator extends Decorator {
    public BracketsDecorator(Component component) {
        super(component);
    }

    @Override
    public void doOperation() {
        openBracket();
        super.doOperation();
        closeBracket();
    }

    private void openBracket() {
        System.out.print("<< ");
    }

    private void closeBracket() {
        System.out.print(" >>");
    }
}
