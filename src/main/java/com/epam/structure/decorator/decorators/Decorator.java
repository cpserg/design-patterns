package com.epam.structure.decorator.decorators;

import com.epam.structure.decorator.Component;

/**
 *  декоратор:
 * - хранит ссылку на объект Component и определяет интерфейс, соответствующий интерфейсу Component;
 */
public abstract class Decorator implements Component {
    protected Component component;

    public Decorator(Component component) {
        this.component = component;
    }

    @Override
    public void doOperation() {
        component.doOperation();
    }

    public void newOperation() {
        System.out.println("");
        System.out.println("new_operation");
    }
}
