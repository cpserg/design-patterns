package com.epam.structure.decorator;

import com.epam.structure.decorator.decorators.BracketsDecorator;
import com.epam.structure.decorator.decorators.Decorator;
import com.epam.structure.decorator.decorators.QuotesDecorator;

/**
 * Декоратор (Обертка)
 *
 * Динамически добавляет объекту новые обязанности.
 * Является альтернативой порождению подклассов с целью расширения функциональности.
 */
public class Client {
    public static void main(String[] args) {
        SimpleComponent component = new SimpleComponent();
        Decorator decorator = new BracketsDecorator(new QuotesDecorator(component));
        decorator.doOperation();
        decorator.newOperation();

    }
}
