package com.epam.structure.decorator;

/**
 * интерфейс для объектов, на которые могут быть динамически возложены дополнительные обязанности
 */
public interface Component {
    void doOperation();
}
