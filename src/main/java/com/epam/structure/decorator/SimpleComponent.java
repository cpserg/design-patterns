package com.epam.structure.decorator;

/**
 *  определяет объект, на который возлагаются дополнительные обязанности
 */
public class SimpleComponent implements Component {

    @Override
    public void doOperation() {
        System.out.print("simple component");
    }
}
