package com.epam.structure.composite;

/**
 * Компоновщик
 *
 * Компонует объекты в древовидные структуры для представления иерархий часть-целое.
 * Позволяет клиентам единообразно трактовать индивидуальные и составные объекты.
 *
 */
public class Client {

    public static void main(String[] args) {
        Composite root = new Composite();
        Composite branch = new Composite();
        root.addComponent(branch);

        Leaf leaf_1 = new Leaf();
        Leaf leaf_2 = new Leaf();
        branch.addComponent(leaf_1);
        branch.addComponent(leaf_2);

        root.operation();

    }



}
