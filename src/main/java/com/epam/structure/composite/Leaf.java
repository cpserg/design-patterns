package com.epam.structure.composite;

import java.util.Collection;

/**
 *  Определяет поведение примитивных объектов в композиции.
 *
 */
public class Leaf implements Component {

    @Override
    public void operation() {
        System.out.println("do operation");
    }

    /**
     * Примитивные объекты складываем в коллекцию
     * @param componentList
     */
    @Override
    public void collectComponents(Collection<Component> componentList) {
        componentList.add(this);
    }
}
