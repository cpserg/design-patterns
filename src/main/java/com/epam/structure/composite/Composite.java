package com.epam.structure.composite;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * - определяет поведение компонентов, у которых есть потомки;
 * - хранит компоненты-потомки;
 *
 */
public class Composite implements Component {
    private Set<Component> components = new HashSet<>();

    public void addComponent(Component component) {
        components.add(component);
    }

    public void removeComponent(Component component) {
        components.remove(component);
    }

    @Override
    public void operation() {
        compositeOperation();

        for (Component component: components) {
            component.operation();
        }
    }

    @Override
    public void collectComponents(Collection<Component> componentList) {
        for (Component component: components) {
            component.collectComponents(componentList);
        }
    }

    private void compositeOperation() {
        System.out.println("do operations on components of " + this);
    }
}
