package com.epam.structure.composite;

import java.util.Collection;

/**
 * Интерфейс, определяющий методы, которые должны быть
 * доступными всем частям древовидной структуры.
 */
public interface Component {
    void operation();

    void collectComponents(Collection<Component> componentList);
}
