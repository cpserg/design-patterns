package com.epam.behavioral.mediator;

/**
 * Mediator (Посредник)
 *
 */
public class Client {
    public static void main(String[] args) {
        //посредник
        Mediator mediator = new ConcreteMediator();

        //фермер выращивает томаты
        Colleague fermer = new FermerColleague(mediator);
        //фабрика делает из томатов кетчуп
        Colleague fabric = new FabricColleague(mediator);

        fermer.send("Я вырастил томаты, забирай");
        fabric.send("Томаты переработаны. Забирай кетчуп");

    }
}
