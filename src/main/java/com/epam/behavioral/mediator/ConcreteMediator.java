package com.epam.behavioral.mediator;

/**
 * реализует кооперативное поведение, координируя действия объектов Colleague
 */
public class ConcreteMediator implements Mediator {
    private FermerColleague fermerColleague;
    private FabricColleague fabricColleague;

    @Override
    public void send(String message, Colleague sender) {
        System.out.println(sender + " прислал сообщение: " + message);
        doBusiness(sender);
    }

    private void doBusiness(Colleague sender) {
        if (sender instanceof FermerColleague) {
            System.out.println("фермер прислал томаты.... повезу-ка я их на фабрику.");
        } else if (sender instanceof FabricColleague) {
            System.out.println("ну и что же мне делать с этим кетчупом. придется создать еще коллегу-магазин чтобы продать кетчуп");
        }
    }


}
