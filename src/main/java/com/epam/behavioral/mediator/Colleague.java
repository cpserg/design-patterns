package com.epam.behavioral.mediator;

/**
 * - каждый класс Colleague «знает» о своем объекте Mediator
 */
public abstract class Colleague {
    protected Mediator mediator;

    public Colleague(Mediator mediator) {
        this.mediator = mediator;
    }

    /**
     * - все коллеги обмениваются информацией только с посредником, так как
     *   при его отсутствии им пришлось бы общаться между собой напрямую
     *   this - чтобы посредник знал от кого сообщение
     * @param message
     */
    public void send(String message) {
        mediator.send(message, this);
    }

}
