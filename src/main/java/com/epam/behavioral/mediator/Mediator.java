package com.epam.behavioral.mediator;

/**
 * определяет интерфейс для обмена информацией с объектами Colleague
 */
public interface Mediator {
    void send(String message, Colleague sender);
}
