package com.epam.behavioral.observer;

/**
 * ConcreteSubject - конкретный субъект:
 * - сохраняет состояние, представляющее интерес для конкретного наблюдателя ConcreteObserver;
 * - посылает информацию своим наблюдателям, когда происходит изменение;
 */
public class ConcreteSubject extends Subject {
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
