package com.epam.behavioral.observer;

/**
 * наблюдатель:
 * - определяет интерфейс обновления для объектов, которые должны быть уведомлены об изменении субъекта;
 */
public interface Observer {
    void update();
}
