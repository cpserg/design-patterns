package com.epam.behavioral.observer;

/**
 *
 */
public class Client {
    public static void main(String[] args) {
        //издатель
        ConcreteSubject publisher = new ConcreteSubject();

        //клиенты - подписчики
        Observer client_1 = new ConcreteObserver(publisher);
        Observer client_2 = new ConcreteObserver(publisher);
        Observer client_3 = new ConcreteObserver(publisher);

        publisher.attach(client_1);
        publisher.attach(client_2);
        publisher.attach(client_3);

        //издатель создает новую статью
        publisher.setState("Новая статья готова");
        //и уведомляет подписчиков
        publisher.notifyAllObservers();

        //издатель отписывает двоих от рассылки
        publisher.detach(client_1);
        publisher.detach(client_2);

        publisher.setState("Еще одна Новая статья готова");
        publisher.notifyAllObservers();

    }
}
