package com.epam.behavioral.observer;

/**
 * ConcreteObserver - конкретный наблюдатель:
 * - хранит ссылку на объект класса ConcreteSubject;
 * - сохраняет данные, которые должны быть согласованы с данными субъекта;
 * - реализует интерфейс обновления, определенный в классе Observer, чтобы поддерживать согласованность с субъектом.
 */
public class ConcreteObserver implements Observer {
    private ConcreteSubject concreteSubject;
    private String state;

    public ConcreteObserver(ConcreteSubject concreteSubject) {
        this.concreteSubject = concreteSubject;
        concreteSubject.attach(this);
    }

    @Override
    public void update() {
        state = concreteSubject.getState();
        System.out.println(this + " has updated. State: " + state);
    }
}
