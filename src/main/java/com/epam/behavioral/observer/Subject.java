package com.epam.behavioral.observer;

import java.util.HashSet;
import java.util.Set;

/**
 *  Subject - субъект:
 * - располагает информацией о своих наблюдателях.
 * - за субъектом может «следить» любое число наблюдателей;
 * - предоставляет интерфейс для присоединения и отделения наблюдателей;
 */
public abstract class Subject {
    private Set<Observer> observers = new HashSet<>();

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void detach(Observer observer) {
        observers.remove(observer);
    }

    public void notifyAllObservers() {
        for (Observer observer: observers) {
            observer.update();
        }
    }

}
