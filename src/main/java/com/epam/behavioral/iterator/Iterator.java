package com.epam.behavioral.iterator;

/**
 *  интерфейс для доступа и обхода элементов
 * @param <T>
 */
public interface Iterator<T> {
    T getFirst();
    T getNext();
    boolean isDone();
    T getCurrentItem();

}
