package com.epam.behavioral.iterator;

import com.epam.structure.composite.Leaf;

import java.util.List;

public class LeafIterator implements Iterator<Leaf> {
    private List<Leaf> leafs;
    private int cursor;

    public LeafIterator(List<Leaf> leafs) {
        this.leafs = leafs;
    }

    @Override
    public Leaf getFirst() {
        return leafs == null || leafs.isEmpty() ? null : leafs.get(0);
    }

    @Override
    public Leaf getNext() {
        if (isDone()) {
            return null;
        }
        Leaf leaf = leafs.get(cursor);
        cursor++;
        return leaf;
    }

    @Override
    public boolean isDone() {
        return leafs == null || leafs.isEmpty() || cursor >= leafs.size();
    }

    @Override
    public Leaf getCurrentItem() {
        return leafs == null || leafs.isEmpty() ? null : leafs.get(cursor);
    }


}
