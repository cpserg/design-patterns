package com.epam.behavioral.iterator;

import com.epam.structure.composite.Composite;
import com.epam.structure.composite.Leaf;

/**
 * Iterator (Cursor)
 * - Когда у вас есть сложная структура данных, и вы хотите скрыть от клиента детали её реализации (из-за сложности или вопросов безопасности).
 * - Когда вам нужно иметь несколько вариантов обхода одной и той же структуры данных
 * - Когда вам хочется иметь единый интерфейс обхода различных структур данных
 * - Не оправдан, если можно обойтись простым циклом
 *
 * Реализация
 * - какой участник управляет итерацией.
 *   Важнейший вопрос состоит в том, что управляет итерацией: сам итератор или клиент, который им пользуется.
 *   Если итерацией управляет клиент, то итератор называется внешним, в противном случае - внутренним
 * - что определяет алгоритм обхода. Алгоритм обхода можно определить не только в итераторе.
 *   Его может определить сам агрегат и использовать итератор только для хранения состояния итерации. Такого рода итератор мы
 *   называем курсором, поскольку он всего лишь указывает на текущую позицию в агрегате
 * - насколько итератор устойчив
 *   Модификация агрегата в то время, как совершается его обход, может оказаться опасной
 *   Простое решение - скопировать агрегат и обходить копию, но обычно это слишком дорого
 * - Устойчивый итератор (robust)
 *   При вставке или удалении агрегат либо подправляет внутреннее состояние всех созданных им итераторов, либо
 *   организует внутреннюю информацию так, чтобы обход выполнялся правильно
 * - итераторы могут иметь привилегированный доступ
 * - итераторы для составных объектов
 *   (таких, например, которые возникают в результате применения паттерна компоновщик)
 * - пустые итераторы
 *   Применение пустого итератора может упростить обход древовидных структур (например, объектов Composite).
 *   В каждой точке обхода мы запрашиваем у текущего элемента итератор для его потомков. Элементы-агрегаты,
 *   как обычно, возвращают конкретный итератор. Но листовые элементы возвращают экземпляр Nulllterator.
 *   Это позволяет реализовать обход всей структуры единообразно
 *
 */
public class Example {
    public static void main(String[] args) {
        //create a Tree with two branch and two leafs on each branch;
        Composite tree = new Composite();
        Composite root = new Composite();
        tree.addComponent(root);
        root.addComponent(new Leaf()); //1
        root.addComponent(new Leaf()); //2
        Composite branch = new Composite();
        root.addComponent(branch);
        branch.addComponent(new Leaf());//3
        branch.addComponent(new Leaf());//4

        LeafAggregate leafAggregate = new LeafAggregate(tree);
        Iterator<Leaf> leafIterator = leafAggregate.createIterator();
        System.out.println(leafIterator.getFirst());
        System.out.println(leafIterator.getCurrentItem());
        System.out.println(leafIterator.isDone());
        while (!leafIterator.isDone()) {
            System.out.println(leafIterator.getNext());
        }
        System.out.println(leafIterator.isDone());




    }
}
