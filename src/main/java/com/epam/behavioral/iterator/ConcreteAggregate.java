package com.epam.behavioral.iterator;

/**
 * реализует интерфейс создания итератора и возвращает экземпляр подходящего класса Concretelterator
 * хранит массив элементов которые итератор будет обходить
 */
public class ConcreteAggregate<T> implements Aggregate {

    @Override
    public Iterator<T> createIterator() {
        return new ConcreteIterator();
    }
}
