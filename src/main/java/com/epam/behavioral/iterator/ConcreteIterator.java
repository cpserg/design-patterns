package com.epam.behavioral.iterator;

/**
 * следит за текущей позицией при обходе агрегата
 * Concretelterator отслеживает текущий объект в агрегате и может вычислить идущий за ним.
 */
public class ConcreteIterator implements Iterator {
    /**
     * инициализирует текущий элемент первым элементом списка
     * @return
     */
    @Override
    public Object getFirst() {
        return null;
    }

    /**
     * делает текущим следующий элемент
     * @return
     */
    @Override
    public Object getNext() {
        return null;
    }

    /**
     * проверяет, не оказались ли мы за последним элементом, если да, то обход завершен
     * @return
     */
    @Override
    public boolean isDone() {
        return false;
    }

    /**
     * возвращает текущий элемент списка
     * @return
     */
    @Override
    public Object getCurrentItem() {
        return null;
    }
}
