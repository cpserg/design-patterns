package com.epam.behavioral.iterator;

/**
 * Aggregator (Container)
 * определяет интерфейс для создания объекта-итератора

 */
public interface Aggregate {
    Iterator createIterator();
}
