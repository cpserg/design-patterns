package com.epam.behavioral.iterator;

import com.epam.structure.composite.Component;
import com.epam.structure.composite.Composite;
import com.epam.structure.composite.Leaf;

import java.util.ArrayList;
import java.util.List;

public class LeafAggregate implements Aggregate {
    private List<Leaf> leafs = new ArrayList<>();

    public LeafAggregate(Composite tree) {
        List<Component> components = new ArrayList<>();
        tree.collectComponents(components);
        for (Component component: components) {
            leafs.add((Leaf) component);
        }
    }

    @Override
    public LeafIterator createIterator() {
        return new LeafIterator(leafs);
    }
}
