package com.epam.behavioral.visitor;

public class ConcreteVisitor2 implements Visitor {
    @Override
    public void visitConcreteElementA(ConcreteElementA concreteElementA) {
        System.out.println("ConcreteVisitor 2 has visit ConcreteElement A (троль ворует подарки)");
    }

    @Override
    public void visitConcreteElementB(ConcreteElementB concreteElementB) {
        System.out.println("ConcreteVisitor 2 has visit ConcreteElement B (троль ворует подарки)");
    }
}
