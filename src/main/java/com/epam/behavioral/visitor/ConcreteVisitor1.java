package com.epam.behavioral.visitor;

public class ConcreteVisitor1 implements Visitor {
    @Override
    public void visitConcreteElementA(ConcreteElementA concreteElementA) {
        System.out.println("ConcreteVisitor 1 has visit ConcreteElement A (дед мороз дарит подарки)");
    }

    @Override
    public void visitConcreteElementB(ConcreteElementB concreteElementB) {
        System.out.println("ConcreteVisitor 1 has visit ConcreteElement B (дед мороз дарит подарки)");
    }
}
