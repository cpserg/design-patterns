package com.epam.behavioral.visitor;

/**
 * определяет операцию Accept, которая принимает посетителя в качестве аргумента
 */
public interface Element {
    void accept(Visitor visitor);
}
