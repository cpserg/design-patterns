package com.epam.behavioral.visitor;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * структура объектов
 * - может быть как составным объектом (см. паттерн компоновщик), так и коллекцией, например списком или множеством
 * - может перечислить свои элементы;
 * - может предоставить посетителю высокоуровневый интерфейс для посещения своих элементов;
 */
public class ObjectStructure {
    Set<Element> elements = new HashSet<>();

    public void add(Element element) {
        elements.add(element);
    }

    public void remove(Element element) {
        elements.remove(element);
    }

    public Iterator<Element> getIterator() {
        return elements.iterator();
    }
}
