package com.epam.behavioral.visitor;

/**
 * реализует операцию Accept, принимающую посетителя как аргумент
 */
public class ConcreteElementA implements Element {
    @Override
    public void accept(Visitor visitor) {
        visitor.visitConcreteElementA(this);
    }
}
