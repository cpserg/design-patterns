package com.epam.behavioral.visitor;

/**
 * объявляет операцию Visit для каждого класса ConcreteElement в структуре объектов.
 * Имя и сигнатура этой операции идентифицируют  класс, который посылает посетителю запрос Visit.
 * Это позволяет посетителю определить, элемент какого конкретного класса он посещает.
 * Владея такой информацией, посетитель может обращаться к элементу напрямую через его интерфейс
 */
public interface Visitor {
    void visitConcreteElementA(ConcreteElementA concreteElementA);

    void visitConcreteElementB(ConcreteElementB concreteElementB);
}
