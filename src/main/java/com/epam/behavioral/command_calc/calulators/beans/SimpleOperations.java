package com.epam.behavioral.command_calc.calulators.beans;

/**
 *  Содержит бизнес-логику программы.
 *  Располагает информацией о способах выполнения операций, необходимых для удовлетворения запроса.
 *  В роли получателя может выступать любой класс
 *
 */
public class SimpleOperations {

    public Number plus(Number number, Number result) {
        return result.doubleValue() + number.doubleValue();
    }

    public Number minus(Number number, Number result) {
        return result.doubleValue() - number.doubleValue();
    }

}
