package com.epam.behavioral.command_calc.calulators;

import com.epam.behavioral.command_calc.calulators.beans.SimpleOperations;

public class Calculator {
    private Number result;
    private SimpleOperations operations = new SimpleOperations();

    public Number getResult() {
        return result;
    }

    public void inputNumber(Number number) {
        result = number;
    }

    public void minus(Number number) {
        result = operations.minus(number, result);
    }

    public void plus(Number number) {
        result = operations.plus(number, result);
    }
}
