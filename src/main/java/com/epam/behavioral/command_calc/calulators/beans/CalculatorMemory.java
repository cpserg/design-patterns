package com.epam.behavioral.command_calc.calulators.beans;

public class CalculatorMemory {
    private Number number;

    public Number getNumber() {
        return number;
    }

    public void setNumber(Number number) {
        this.number = number;
    }
}
