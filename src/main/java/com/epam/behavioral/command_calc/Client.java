package com.epam.behavioral.command_calc;

import com.epam.behavioral.command_calc.calulators.Calculator;
import com.epam.behavioral.command_calc.commands.*;

/**
 * Command
 *
 * - Когда вы хотите ставить операции в очередь, выполнять их по расписанию или передавать по сети.
 * - Когда вам нужна операция отмены.
 * - Когда вы хотите параметризовать объекты выполняемым действием.
 *
 */
public class Client {

    public static void main(String[] args) {

        //класс с бизнес-логикой
        //todo: получение калькулятора фабричным методом
        Calculator calculator = new Calculator();

        //создаем объекты-команды
        Command command_1 = new InputCommand(calculator, 11);
        Command command_2 = new PlusCommand(calculator, 5);
        Command command_3 = new MinusCommand(calculator, 1);
        Command command_4 = new PrintResultCommand(calculator);

        //регистрируем команды
        Invoker invoker = new Invoker();
        invoker.register("command_1", command_1);
        invoker.register("command_2", command_2);
        invoker.register("command_3", command_3);
        invoker.register("command_4", command_4);

        //выполняем
        invoker.execute("command_1");
        invoker.execute("command_2");
        //invoker.undo("command_2");
        invoker.execute("command_3");
        invoker.execute("command_4");

    }
}
