package com.epam.behavioral.command_calc.commands;

import com.epam.behavioral.command_calc.calulators.Calculator;

public abstract class Command {
    protected Calculator receiver;

    public abstract void execute();

    public void undo(String commandName) {
        //каждаая команда может определить свои действия при отмене
    }

}
