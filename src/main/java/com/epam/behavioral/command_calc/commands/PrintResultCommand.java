package com.epam.behavioral.command_calc.commands;

import com.epam.behavioral.command_calc.calulators.Calculator;

public class PrintResultCommand extends Command {

    public PrintResultCommand(Calculator receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        System.out.println(receiver.getResult());
    }
}
