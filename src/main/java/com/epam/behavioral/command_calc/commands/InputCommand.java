package com.epam.behavioral.command_calc.commands;

import com.epam.behavioral.command_calc.calulators.Calculator;

public class InputCommand extends Command {
    private Number number;

    public InputCommand(Calculator receiver, Number number) {
        this.receiver = receiver;
        this.number = number;
    }

    @Override
    public void execute() {
        receiver.inputNumber(number);
    }
}
