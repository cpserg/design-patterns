package com.epam.behavioral.command_calc.commands;

import com.epam.behavioral.command_calc.calulators.Calculator;

public class MinusCommand extends Command {
    private Number parameters;

    public MinusCommand(Calculator receiver, Number parameters) {
        this.receiver = receiver;
        this.parameters = parameters;
    }

    @Override
    public void execute() {
        receiver.minus(parameters);
    }
}
