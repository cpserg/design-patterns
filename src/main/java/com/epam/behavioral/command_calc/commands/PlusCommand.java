package com.epam.behavioral.command_calc.commands;

import com.epam.behavioral.command_calc.calulators.Calculator;

public class PlusCommand extends Command {
    private Number number;

    public PlusCommand(Calculator receiver, Number number) {
        this.receiver = receiver;
        this.number = number;
    }

    @Override
    public void execute() {
        receiver.plus(number);
    }
}
