package com.epam.behavioral.chain;

public abstract class Handler {
    public static final String CHECK_REQUEST_TO_HANDLE = "Check request to handle ";
    private Handler successor;

    public abstract void handleRequest(int request);

    public Handler getSuccessor() {
        return successor;
    }

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    protected void handleNext(int request) {
        if (successor != null) {
            successor.handleRequest(request);
        }
    }

}
