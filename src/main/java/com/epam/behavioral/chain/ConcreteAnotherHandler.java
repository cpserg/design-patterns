package com.epam.behavioral.chain;

public class ConcreteAnotherHandler extends Handler {
    @Override
    public void handleRequest(int request) {
        System.out.println(CHECK_REQUEST_TO_HANDLE + 2);
        if (request == 2) {
            System.out.println("process...  two");
        } else {
            handleNext(request);
        }
    }
}
