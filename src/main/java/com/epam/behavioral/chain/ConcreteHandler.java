package com.epam.behavioral.chain;

public class ConcreteHandler extends Handler {

    @Override
    public void handleRequest(int request) {
        System.out.println(CHECK_REQUEST_TO_HANDLE + 1);
        if (request == 1) {
            System.out.println("process...  one");
        } else {
            handleNext(request);
        }
    }

}
