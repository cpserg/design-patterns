package com.epam.behavioral.chain;

/**
 * Chain of responsibilities
 *
 * - есть более одного объекта, способного обработать запрос, причем настоящий обработчик заранее неизвестен и должен быть найден автоматически;
 * - вы хотите отправить запрос одному из нескольких объектов, не указывая явно, какому именно;
 * - набор объектов, способных обработать запрос, должен задаваться динамически
 *
 * может блокировать всю цепочку если приходит много запросов и процесс их обработки долгий
 */
public class Client {

    public static void main(String[] args) {
        Handler firstHandlerInChain = new ConcreteHandler();
        Handler secondHandlerInChain = new ConcreteAnotherHandler();

        firstHandlerInChain.setSuccessor(secondHandlerInChain);

        int request = 1;
        firstHandlerInChain.handleRequest(request);

        request = 2;
        firstHandlerInChain.handleRequest(request);

    }

}
