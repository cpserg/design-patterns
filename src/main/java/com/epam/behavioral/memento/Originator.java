package com.epam.behavioral.memento;

/**
 * Хозяин
 * - создает хранителя, содержащего снимок текущего внутреннего состояния;
 * - использует хранителя для восстановления внутреннего состояния;
 */

public class Originator {
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void restoreStateFromMemento(Memento memento) {
        state = memento.getState();
    }

    public Memento createMemento() {
        return new Memento(state);
    }
}
