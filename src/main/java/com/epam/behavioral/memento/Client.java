package com.epam.behavioral.memento;

/**
 * Memento (Хранитель, Token)
 * Иногда необходимо тем или иным способом зафиксировать внутреннее состояние объекта
 * например, при реализации контрольных точек и механизмов отката
 *
 * Метафора: вы (Originator, хозяин) купили робота Asimo (Caretaker, Посыльный),
 * чтобы он охранял вашу одежду (ваше состояние) пока вы купаетесь на пляже.
 * Робот не умеет открывать рюкзак и смотреть что внутри (исследовать состояние)
 *
 */
public class Client {
    public static void main(String[] args) {

        Originator originator = new Originator();

        originator.setState("Майка, джинсы, кеды");

        Caretaker backpack = new Caretaker();
        backpack.add(originator.createMemento());//добавили робота с одеждой в рюкзак

        originator.setState("Плавки");
        backpack.add(originator.createMemento());//добавили робота с плавки в рюкзак

        //Идем купаться
        System.out.println("Current State: " + originator.getState());

        //Покупались, переодеваемся
        originator.restoreStateFromMemento(backpack.get(0));
        System.out.println("First saved State: " + originator.getState());


    }
}
