package com.epam.behavioral.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * Caretaker (механизм отката) - посыльный (рюкзак)
 * - отвечает за сохранение хранителя;
 * - не производит никаких операций над хранителем и не исследует его внутреннее содержимое.
 */
public class Caretaker {
    private List<Memento> mementoList = new ArrayList<>();

    public void add(Memento state){
        mementoList.add(state);
    }

    public Memento get(int index){
        return mementoList.get(index);
    }
}
