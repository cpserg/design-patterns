package com.epam.behavioral.template_method;

public class UkraineFlag extends AbstractFlag {

    @Override
    public void drawTopPart() {
        System.out.println(Color.BLUE);
    }

    @Override
    public void drawBottomPart() {
        System.out.println(Color.YELLOW);
    }
}
