package com.epam.behavioral.template_method;

public enum Color {
    WHITE("WHITE "), RED(" RED  "), BLUE(" BLUE "), YELLOW("YELLOW");

    private String color;

    Color(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "+------------+\n|   " + color + "   |\n+------------+";
    }
}
