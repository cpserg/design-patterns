package com.epam.behavioral.template_method;

/**
 * AbstractClass
 * - определяет абстрактные примитивные операции, замещаемые в конкретных подклассах для реализации шагов алгоритма;
 * - реализует шаблонный метод, определяющий скелет алгоритма. Шаблонный метод вызывает примитивные операции,
 *   а также операции, определенные в классе AbstractClass или в других объектах;
 */
public abstract class AbstractFlag {
    protected void templateMethod() {
        drawTopPart();
        drawBottomPart();
    }
    public abstract void drawTopPart();
    public abstract void drawBottomPart();

}
