package com.epam.behavioral.template_method;

/**
 * ConcreteClass
 * - реализует примитивные операции, выполняющие шаги алгоритма способом, который зависит от подкласса.
 */
public class PolandFlag extends AbstractFlag {
    @Override
    public void drawTopPart() {
        System.out.println(Color.WHITE);
    }

    @Override
    public void drawBottomPart() {
        System.out.println(Color.RED);
    }
}
