package com.epam.behavioral.state;

/**
 *  контекст:
 * - определяет интерфейс, представляющий интерес для клиентов;
 * - хранит экземпляр подкласса ConcreteState, которым определяется текущее состояние;
 * - делегирует зависящие от состояния запросы текущему объекту ConcreteState
 * - контекст может передать себя в качестве аргумента объекту State, который будет обрабатывать запрос. Это дает возможность объекту-состоянию
 *   при необходимости получить доступ к контексту;
 * - либо Context, либо подклассы ConcreteState могут решить, при каких
 *   условиях и в каком порядке происходит смена состояний
 */
public class Context {
    private State state;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void request() {
        state.handle(this);
    }

}
