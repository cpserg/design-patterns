package com.epam.behavioral.state;

/**
 * конкретное состояние:
 * - каждый подкласс реализует поведение, ассоциированное с некоторым состоянием контекста Context
 */
public class ConcreteStateA implements State {
    @Override
    public void handle(Context context) {
        System.out.println("Handle state A");
    }
}
