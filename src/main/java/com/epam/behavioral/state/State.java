package com.epam.behavioral.state;

/**
 * состояние:
 * - определяет интерфейс для инкапсуляции поведения, ассоциированного
 *   с конкретным состоянием контекста Context;
 */
public interface State {
    void handle(Context context);
}
