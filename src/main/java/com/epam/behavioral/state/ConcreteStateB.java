package com.epam.behavioral.state;

/**
 * конкретное состояние:
 * - каждый подкласс реализует поведение, ассоциированное с некоторым состоянием контекста Context
 */
public class ConcreteStateB implements State {
    @Override
    public void handle(Context context) {
        System.out.println("Handle state B");
        //States are allowed to replace themselves (IE: to change the state of the context object to something else),
        // while Strategies are not
        context.setState(new ConcreteStateB());
    }
}
