package com.epam.behavioral.command;

import com.epam.behavioral.command.commands.AnotherConcreteCommand;
import com.epam.behavioral.command.commands.Command;
import com.epam.behavioral.command.commands.ConcreteCommand;

/**
 * Command
 *
 * - Когда вы хотите ставить операции в очередь, выполнять их по расписанию или передавать по сети.
 * - Когда вам нужна операция отмены.
 * - Когда вы хотите параметризовать объекты выполняемым действием.
 *
 */
public class Client {

    public static void main(String[] args) {

        //класс с бизнес-логикой
        Receiver receiver = new Receiver();

        //создаем объекты-команды
        Command command = new ConcreteCommand(receiver, 1);
        Command command1 = new AnotherConcreteCommand(receiver);

        //регистрируем команды
        Invoker invoker = new Invoker();
        invoker.register("command", command);
        invoker.register("command_1", command1);

        //выполняем
        invoker.execute("command");
        invoker.execute("command_1");
        //или отменяем
        invoker.undo("command");
//        invoker.execute("command"); бросит ошибку т.к. команда отменена

    }
}
