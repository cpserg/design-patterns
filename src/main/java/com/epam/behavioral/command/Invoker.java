package com.epam.behavioral.command;

import com.epam.behavioral.command.commands.Command;

import java.util.HashMap;

/**
 *  Invoker (Инициатор)
 *  сохраняет объект ConcreteCommand.
 *
 *  Если поддерживается отмена выполненных действий, то ConcreteCommand перед вызовом Execute сохраняет информацию о состоянии,
 *  достаточную для выполнения отката;
 *
 */
public class Invoker {
    private final HashMap<String, Command> commandMap = new HashMap<>();

    public void register(String commandName, Command command) {
        commandMap.put(commandName, command);
    }

    public void undo(String commandName) {
        Command command = getCommand(commandName);
        command.undo(commandName);
        commandMap.remove(commandName);
    }

    public void execute(String commandName) {
        Command command = getCommand(commandName);
        command.execute();
    }

    private Command getCommand(String commandName) {
        Command command = commandMap.get(commandName);
        if (command == null) {
            throw new IllegalStateException("no command registered for " + commandName);
        }
        return command;
    }

}
