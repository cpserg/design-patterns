package com.epam.behavioral.command;

/**
 *  Содержит бизнес-логику программы.
 *  Располагает информацией о способах выполнения операций, необходимых для удовлетворения запроса.
 *  В роли получателя может выступать любой класс
 *
 */
public class Receiver {

    public void operation() {
        System.out.println("do operation");
    }

    public void operation(int param) {
        System.out.println("do operation with parameter " + param);
    }
}
