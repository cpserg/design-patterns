package com.epam.behavioral.command.commands;

import com.epam.behavioral.command.Receiver;

public abstract class Command {
    protected Receiver receiver;

    public abstract void execute();

    public void undo(String commandName) {
        //каждаая команда может определить свои действия при отмене
    }

}
