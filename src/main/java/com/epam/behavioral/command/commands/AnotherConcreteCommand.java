package com.epam.behavioral.command.commands;

import com.epam.behavioral.command.Receiver;

public class AnotherConcreteCommand extends Command {

    public AnotherConcreteCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.operation();
    }
}
