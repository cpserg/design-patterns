package com.epam.behavioral.command.commands;

import com.epam.behavioral.command.Receiver;

public class ConcreteCommand extends Command {
    private int parameters;

    public ConcreteCommand(Receiver receiver, int parameters) {
        this.receiver = receiver;
        this.parameters = parameters;
    }

    @Override
    public void execute() {
        receiver.operation(parameters);
    }
}
