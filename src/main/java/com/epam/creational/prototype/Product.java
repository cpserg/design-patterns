package com.epam.creational.prototype;

/**
 * Java language has already implemented the Prototype pattern by declaration Cloneable interface
 */
public class Product implements Cloneable {
    private String name;
    private int weight;

    public Product() {
        super();
    }

    public Product(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    protected Object clone() {
        Product copy = new Product();
        copy.setName(name);
        copy.setWeight(weight);
        return copy;
    }

    @Override
    public String toString() {
        return "Product{" +
                "hashCode='" + this.hashCode() + '\'' +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
