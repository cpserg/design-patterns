package com.epam.creational.prototype;

public class ProductFactory {
    private  Product product;

    public ProductFactory(Product product) {
        this.product = product;
    }

    public Product makeCopy() throws CloneNotSupportedException {
        return (Product) this.product.clone();
    }
}
