package com.epam.creational.prototype;

public class ClientCode1 {

    /**
     * Здесь рассмотрена реализация патерна Прототип
     * с фабрикой, которой передается объект для последующего клонирования
     * @param args
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        Product product = new Product("cookie", 1);

        ProductFactory productFactory = new ProductFactory(product);
        for (int i = 0; i < 10; i++) {
            System.out.println(i + ": " + productFactory.makeCopy());
        }


    }
}
