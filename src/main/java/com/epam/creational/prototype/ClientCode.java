package com.epam.creational.prototype;

public class ClientCode {

    /**
     * Здесь рассмотрена реализация патерна Прототип
     * @param args
     */
    public static void main(String[] args) {
        Product product = new Product("cookie", 1);
        Product product1 = (Product) product.clone();

        System.out.println(product);
        System.out.println(product1);

    }
}
