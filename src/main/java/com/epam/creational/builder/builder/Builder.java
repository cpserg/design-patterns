package com.epam.creational.builder.builder;

/**
 * задает абстрактный интерфейс для создания частей объекта Product
 */
public interface Builder {
    void init();

    void setSeats(int seats);

    void setEngine(String engine);

    void setTripComputer(String tripComputer);

    void setGPS(boolean isGPS);
}
