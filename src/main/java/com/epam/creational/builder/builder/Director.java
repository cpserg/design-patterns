package com.epam.creational.builder.builder;

public class Director {
    Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    /**
     * конструирует объект, пользуясь интерфейсом Builder
     */
    public void construct() {
        builder.init();
        builder.setEngine("eng");
        builder.setGPS(true);
        builder.setSeats(4);
        builder.setTripComputer("comp");
    }
}
