package com.epam.creational.builder.builder;

import com.epam.creational.builder.Car;

/**
 * конструирует и собирает вместе части продукта посредством реализации
 * интерфейса Builder
 */
public class CarBuilder implements Builder {
    private Car car;

    /**
     * предоставляет интерфейс для доступа к продукту
     */
    public Car getResult() {
        return car;
    }

    @Override
    public void init() {
        car = new Car();
    }

    @Override
    public void setSeats(int seats) {
        car.setSeats(seats);
    }

    @Override
    public void setEngine(String engine) {
        car.setEngine(engine);
    }

    @Override
    public void setTripComputer(String tripComputer) {
        car.setTripComputer(tripComputer);
    }

    @Override
    public void setGPS(boolean isGPS) {
        car.setGPS(isGPS);
    }
}
