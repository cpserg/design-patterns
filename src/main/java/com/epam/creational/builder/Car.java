package com.epam.creational.builder;

public class Car {
    private int seats;
    private String engine;
    private String tripComputer;
    private boolean isGPS;

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getTripComputer() {
        return tripComputer;
    }

    public void setTripComputer(String tripComputer) {
        this.tripComputer = tripComputer;
    }

    public boolean isGPS() {
        return isGPS;
    }

    public void setGPS(boolean isGPS) {
        this.isGPS = isGPS;
    }

    @Override
    public String toString() {
        return "Car{" +
                "seats=" + seats +
                ", engine='" + engine + '\'' +
                ", tripComputer='" + tripComputer + '\'' +
                ", isGPS=" + isGPS +
                '}';
    }
}
