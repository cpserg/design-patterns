package com.epam.creational.builder;

import com.epam.creational.builder.builder.CarBuilder;
import com.epam.creational.builder.builder.Director;

/**
 * Строитель - паттерн, порождающий объекты.
 *
 * Назначение
 * Отделяет конструирование сложного объекта от его представления так,
 * что в результате одного и того же процесса конструирования могут получаться разные представления.
 *
 * Используйте паттерн строитель, когда:
 * - алгоритм создания сложного объекта не должен зависеть от того, из каких
 *   астей состоит объект и как они стыкуются между собой;
 * - процесс конструирования должен обеспечивать различные представления конструируемого объекта.
 *
 */
public class ClientCode {

    public static void main(String[] args) {
        CarBuilder builder = new CarBuilder();
        Director director = new Director(builder);
        director.construct();
        Car car = builder.getResult();
        System.out.println(car);
    }
}
