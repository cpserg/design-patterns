package com.epam.creational.factory_method;

import com.epam.creational.factory_method.factory.BookFactory;
import com.epam.creational.factory_method.factory.Factory;
import com.epam.creational.factory_method.factory.PenFactory;
import com.epam.creational.factory_method.products.Product;

public class Example {
    /**
     * Фабричный метод - паттерн, порождающий классы
     *
     * Назначение:
     * Определяет интерфейс (Product) для создания ОБЪЕКТА (одного), но оставляет подклассам (Creator) решение о том, какой класс инстанцировать.
     * Фабричный метод позволяет классу делегировать инстанцирование подклассам.
     *
     * Применимость:
     * Используйте паттерн фабричный метод, когда:
     * - классу заранее неизвестно, объекты каких классов ему нужно создавать;
     * - класс спроектирован так, чтобы объекты, которые он создает, специфицировались подклассами;
     * - класс делегирует свои обязанности одному из нескольких вспомогательных подклассов,
     *   и вы планируете локализовать знание о том, какой класс принимает эти обязанности на себя
     */

    public static void main(String[] args) {
        Factory[] creators = { new BookFactory(), new PenFactory() };
        for (Factory concreteCreator: creators) {
            Product concreteProduct = concreteCreator.factoryMethod();
            System.out.println(concreteProduct + " is created");
        }
    }
}
