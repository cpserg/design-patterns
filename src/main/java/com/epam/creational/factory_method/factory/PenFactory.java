package com.epam.creational.factory_method.factory;

import com.epam.creational.factory_method.products.Pen;
import com.epam.creational.factory_method.products.Product;

public class PenFactory extends Factory {
    @Override
    public Product factoryMethod() {
        return new Pen();
    }
}
