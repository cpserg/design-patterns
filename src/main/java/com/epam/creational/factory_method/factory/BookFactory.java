package com.epam.creational.factory_method.factory;

import com.epam.creational.factory_method.products.Book;
import com.epam.creational.factory_method.products.Product;

public class BookFactory extends Factory {
    @Override
    public Product factoryMethod() {
        return new Book();
    }
}
