package com.epam.creational.factory_method.factory;

import com.epam.creational.factory_method.products.Product;

public abstract class Factory {

    /**
     * объявляет фабричный метод, возвращающий объект типа Product
     * @return
     */
    public abstract Product factoryMethod();

    /*
     * Factory может также определять реализацию по умолчанию фабричного метода, который возвращает объект ConcreteProduct
     */
//    public Product defaultFactoryMethod() { }


    /**
     * может вызывать фабричный метод для создания объекта Product
     * @return
     */
    public Product getProduct() {
        return factoryMethod();
    }


}
