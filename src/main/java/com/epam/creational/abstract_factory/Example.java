package com.epam.creational.abstract_factory;

import com.epam.creational.abstract_factory.factory.AbstractFactory;
import com.epam.creational.abstract_factory.factory.ClassicFactory;
import com.epam.creational.abstract_factory.factory.ModernFactory;

/**
 * Абстрактная фабрика - паттерн, порождающий объекты.
 *
 * Назначение:
 * Предоставляет интерфейс для создания СЕМЕЙСТВ ВЗАИМОСВЯЗАННЫХ или взаимозависимых объектов,
 * не специфицируя их конкретных классов
 *
 * Применимость:
 * Используйте паттерн абстрактная фабрика, когда:
 * - система не должна зависеть от того, как создаются, компонуются и представляются входящие в нее объекты;
 * - входящие в семейство взаимосвязанные объекты должны использоваться вместе
 *   и вам необходимо обеспечить выполнение этого ограничения;
 * - система должна конфигурироваться одним из семейств составляющих ее объектов;
 * - вы хотите предоставить библиотеку объектов, раскрывая только их интерфейсы, но не реализацию.
 */
public class Example {

    public static void main(String[] args) {
        AbstractFactory modernFactory = new ModernFactory();
        Client client_1 = new Client(modernFactory);

        AbstractFactory classicFactory = new ClassicFactory();
        Client client_2 = new Client(classicFactory);

        System.out.println(client_1);
        System.out.println(client_2);

    }
}
