package com.epam.creational.abstract_factory.factory;

import com.epam.creational.abstract_factory.products.AbstractChair;
import com.epam.creational.abstract_factory.products.AbstractCoffeeTable;
import com.epam.creational.abstract_factory.products.AbstractSofa;
import com.epam.creational.abstract_factory.products.modern.ModernChair;
import com.epam.creational.abstract_factory.products.modern.ModernCoffeeTable;
import com.epam.creational.abstract_factory.products.modern.ModernSofa;

/**
 * Реализует операции, создающие конкретные объекты - продукты
 */
public class ModernFactory implements AbstractFactory {
    @Override
    public AbstractChair createChair() {
        return new ModernChair();
    }

    @Override
    public AbstractCoffeeTable createCoffeeTable() {
        return new ModernCoffeeTable();
    }

    @Override
    public AbstractSofa createSofa() {
        return new ModernSofa();
    }
}
