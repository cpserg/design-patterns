package com.epam.creational.abstract_factory.factory;

import com.epam.creational.abstract_factory.products.AbstractChair;
import com.epam.creational.abstract_factory.products.AbstractCoffeeTable;
import com.epam.creational.abstract_factory.products.AbstractSofa;
import com.epam.creational.abstract_factory.products.classic.ClassicChair;
import com.epam.creational.abstract_factory.products.classic.ClassicCoffeeTable;
import com.epam.creational.abstract_factory.products.classic.ClassicSofa;

/**
 * Реализует операции, создающие конкретные объекты - продукты
 */
public class ClassicFactory implements AbstractFactory {
    @Override
    public AbstractChair createChair() {
        return new ClassicChair();
    }

    @Override
    public AbstractCoffeeTable createCoffeeTable() {
        return new ClassicCoffeeTable();
    }

    @Override
    public AbstractSofa createSofa() {
        return new ClassicSofa();
    }
}
