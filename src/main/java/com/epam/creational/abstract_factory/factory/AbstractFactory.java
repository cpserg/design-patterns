package com.epam.creational.abstract_factory.factory;

import com.epam.creational.abstract_factory.products.AbstractChair;
import com.epam.creational.abstract_factory.products.AbstractCoffeeTable;
import com.epam.creational.abstract_factory.products.AbstractSofa;

/**
 * Объявляет интерфейс для операций, создающих абстрактные объекты - продукты
 */
public interface AbstractFactory {
    AbstractChair createChair();
    AbstractCoffeeTable createCoffeeTable();
    AbstractSofa createSofa();
}
