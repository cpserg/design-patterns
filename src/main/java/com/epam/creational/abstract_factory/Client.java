package com.epam.creational.abstract_factory;

import com.epam.creational.abstract_factory.factory.AbstractFactory;
import com.epam.creational.abstract_factory.products.AbstractChair;
import com.epam.creational.abstract_factory.products.AbstractCoffeeTable;
import com.epam.creational.abstract_factory.products.AbstractSofa;

public class Client {
    AbstractChair abstractChair;
    AbstractCoffeeTable abstractCoffeeTable;
    AbstractSofa abstractSofa;

    public Client(AbstractFactory abstractFactory) {
        this.abstractChair = abstractFactory.createChair();
        this.abstractCoffeeTable = abstractFactory.createCoffeeTable();
        this.abstractSofa = abstractFactory.createSofa();
    }

    @Override
    public String toString() {
        return "Client{" +
                "chair=" + abstractChair.getClass().getSimpleName() +
                ", coffeeTable=" + abstractCoffeeTable.getClass().getSimpleName() +
                ", sofa=" + abstractSofa.getClass().getSimpleName() +
                '}';
    }
}
